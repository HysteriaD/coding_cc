#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<math.h>
int main()
{
	int i;
	for (i = 0; i < 10000; i++)
	{
		int n = 1;//利用n来计算数字的位数
		int t = i;
		while (t / 10)
		{
			n++;
			t = t / 10;

		}
		t = i;
		int sum = 0;
		while (t)
		{			
			sum += pow(t % 10, n);//pow用来求次方数
			t = t / 10;
		}
		if (sum == i)
		{
			printf("%d ", i);
		}
	}
	return 0;
}
