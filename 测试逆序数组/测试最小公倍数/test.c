#include <stdio.h>
#include <string.h>
char change(char* x, char* y)
{
    while (x < y)
    {
        {
            int tmp = *x;
            *x = *y;
            *y = tmp;
            x++;
            y--;
        }
    }
}
int main()
{
    char arr[20] = { 0 };
    gets(arr);
    char* left = arr;
    int len = strlen(arr);
    char* right = arr + len - 1;
    change(left, right);
    printf("%s\n", arr);
    return  0;
}