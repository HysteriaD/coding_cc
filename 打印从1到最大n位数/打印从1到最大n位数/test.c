#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
int* printNumbers(int n, int* returnSize)
{
    // write code here
    *returnSize = pow(10, n) - 1;
    int* arr = (int*)malloc(sizeof(int) * (*returnSize));
    for (int i = 0; i < *returnSize; i++)
    {
        arr[i] = i + 1;
    }
    return arr;
}

int main()
{
    int n = 0;
    scanf("%d", &n);
    int returnSize = 0;
    int* arr = printNumbers(n, &returnSize);
    for (int i = 0; i < returnSize; i++)
    {
        printf("%d ", arr[i]);
    }
}
