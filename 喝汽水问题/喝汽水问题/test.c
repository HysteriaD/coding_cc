#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int drink(int money) 
{
	int empty = money;
	int a = money;
	while (empty > 1) 
	{
		a = a + empty / 2;
		empty = empty / 2 + empty % 2;
	}
	return a;
}
int main() 
{
	int money = 0;
	scanf("%d", &money);
	printf("您一共可以喝 %d 瓶饮料\n", drink(money));
	return 0;
}
