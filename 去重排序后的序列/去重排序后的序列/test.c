// 9 - 27
// 第一行包含一个正整数n，表示序列有n个数，接下来有n行
// 每行一个正整数k，为序列中每一个元素的值(1<=N<=10^5,1<=K<=N)
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>//memset的头文件
int main()
{
	int arr[100001] = { 0 };
	int n = 0;
	scanf("%d", &n);
	//  memset(arr, 0, n * sizeof(int));//将这么多字节都改成0,如果使用变长数组arr[n]的话
	int i = 0;
	int m = 0;
	for (i = 0; i < n; i++)
	{
		scanf("%d", &m);
		arr[m] = 1;
	}
	// 我们的思路是将输入的数字，比如说2就把2的位置改为1
	for (i = 1; i <= 100000; i++)// 从1开始是因为我们k的范围是1<=n
	{
		if (arr[i] != 0)
		{
			printf("%d ", i);
		}
	}
	return 0;
}
// 空间换取时间的做法，这种方法比价费空间