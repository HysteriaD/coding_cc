#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

#define SWAP_BIT(n) n=((n&0xaaaaaaaa)>>1) + ((n&0x55555555)<<1)
                //得到偶数位向右移动1位   得到奇数位向左移动1位
int main()
{
	int a = 10;
	//00000000000000000000000000001010 原来是10
	//00000000000000000000000000000101 转换后变成5
	SWAP_BIT(a);
	printf("%d\n", a);
	SWAP_BIT(a);
	printf("%d\n", a);
	return 0;
}

// 0xaaaaaaaa =  101010101010101010101010101010 (偶数位为1，奇数位为0）

// 0x55555555 = 1010101010101010101010101010101 (偶数位为0，奇数位为1）