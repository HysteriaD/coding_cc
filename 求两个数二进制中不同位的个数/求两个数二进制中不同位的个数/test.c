#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int a = 0;
	int b = 0;
	int count = 0;
	scanf("%d %d", &a, &b);
	int c = a ^ b;
	while (c)
	{
		count++;
		c = c & (c - 1);
	}
	printf("%d", count);
	return 0;
}