#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
int GetNumberOfK(int* data, int dataLen, int k) 
{
    // write code here
    scanf("%d", &k);
    int right = dataLen - 1;
    int left = 0;
    int count = 0;
    int ret = 0;
    while (left <= right)
    {
        int mid = (left + right) / 2;
        if (data[mid] < k)         //如果没找到就一直循环找 这里逻辑很简单
        {
            left = mid + 1;
        }
        else if (data[mid] > k)
        {
            right = mid - 1;
        }
        else
        {
            while (1)       //如果找到一个 因为是非降序所以要考虑其左右还有没有，先找其左边
            {
                if (mid >= 0 && data[mid] == k)//因为一会要改变mid 所以要先判断mid防止溢出
                {
                    count++;
                    mid--;
                }
                else
                {
                    mid += count + 1; //如果左边没有k了就恢复mid为最初的mid再+1 跳出
                    break;
                }
            }
            while (mid < dataLen && data[mid] == k)  //判断右边 同理
            {
                if (mid < dataLen && data[mid] == k)
                {
                    count++;
                    mid++;
                }
                else
                    break;
            }
            break;
        }
    }
    return count;
}
