#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//用strcpy必须要加头文件，没加会出现strcpy没有定义的标志
#include <string.h>

int main()
{
	char arr1[20] = "xxxxxxxxxxxxxxx";
	char arr2[] =   "hello bit";
	strcpy(arr1, arr2);
	printf("%s\n", arr1);

	return 0;
}