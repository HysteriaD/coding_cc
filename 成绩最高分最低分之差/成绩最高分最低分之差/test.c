#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int n = 0;
	scanf("%d", &n);
	int max = 0;//假设最大数值为0，如果是100就没有比它大的
	int min = 100;//假设最小值为100
	int i = 0;
	int score = 0;
	for (i = 0; i < n; i++)
	{
		scanf("%d", &score);
		if (score > max)
			max = score;
		if (score < min)
			min = score;
	}
	printf("%d\n", max - min);
}