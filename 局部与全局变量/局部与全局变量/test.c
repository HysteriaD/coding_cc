#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int a = 1;
void test()
{
	int a = 2;//如果讲这句话注释掉，则a+=1会调用全局变量，a=2
	a += 1;
}
int main()
{
	test();
	printf("%d\n", a);
	return 0;
}