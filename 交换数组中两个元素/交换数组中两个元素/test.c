#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int a[5] = { 1,2,3,4,5 };
	int b[5] = { 6,7,8,9,10 };
	int c[5];
	int i, j;
	for (i = 0; i < 5; i++)
	{
		c[i] = a[i];
		a[i] = b[i];
		b[i] = c[i];
	}
	printf("置换后的数组a是 ");
	for (i = 0; i < 5; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
	printf("置换后的数组b是 ");
	for (i = 0; i < 5; i++)
	{
		printf("%d ", b[i]);
	}
	return 0;
}
