#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

/*
*   my_atoi(NULL) 异常
*   my_atoi（“”） 异常
*   my_atoi(" +123") 正常
*   my_atoi ("123abc") 正常
*   my_atoi ("1111111111111111111111111111111") 异常
*/

enum Status
{
	VALID,
	INVALID
};
enum Status status = INVALID;

int my_atoi(const char* str)
{
	if (str == NULL)
	{
		return 0;
	}
	if (*str == '\0')
	{
		return 0;
	}
	// 空白字符
	while (isspace(*str)) //判断是否为空
	{
		str++;
	}
	int flag = 1;
	if (*str == '+')
	{
		flag = 1;
		str++;
	}
	else if (*str == '-')
	{
		flag = -1;
		str++;
	}
	// 处理数字字符
	long long ret = 0;
	while (isdigit(*str))
	{
		ret = ret * 10 + flag*(*str - '0');
		if (ret<INT_MIN || ret > INT_MAX)
		{
			return 0;
		}
		str++;
	}
	if (*str == '\0')
	{
		status = VALID;
		return (int)ret;
	}
	else
	{
		return ret;
	}
}

int main()
{
	int ret = my_atoi("124");
	if (status == VALID)
		printf("合法的转换：%d\n", ret);
	else
		printf("非法的数据转换：%d\n", ret);
	return 0;
}