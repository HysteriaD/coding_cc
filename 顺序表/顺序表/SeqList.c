#include "Seqlist.h"

void SeqlistPrint(SL* ps)
{
	for (int i = 0; i < ps->size; ++i)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

void SeqlistInit(SL* ps)
{
	ps->a = NULL;
	ps->size = ps->capacity = 0;// 初始化
}

void SeqlistDestory(SL* ps)
{
	free(ps->a);
	ps->a = NULL;
	ps->capacity = ps->size = 0;
}

void SeqlistCheckCapacity(SL* ps)
{
	if (ps->size == ps->capacity)
	{
		int newcapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;// 扩容
		SLDataType* tmp = (SLDataType*)realloc(ps->a, newcapacity * sizeof(SLDataType));// realloc算的是字节数
		if (tmp == NULL)
		{
			printf("realloc fail\n");
			exit(-1);// 这里return是不行的exit直接终止程序了
		}

		ps->a = tmp;
		ps->capacity = newcapacity;
	}
}

void SeqlistPushBack(SL* ps, SLDataType x) // 尾插
{
	/*   SeqlistCheckCapacity(ps);
	     ps->a[ps->size] = x;
	     ps->size++; 
	*/
	SeqlistInsert(ps, ps->size, x);
}

void SeqlistPopBack(SL* ps)
{
	
	/*  if (ps->size > 0)
	  {
		//ps->a[ps->size - 1] = 0; 这个不要也没有问题
		ps->size--;
	   }
	*/  // 方法一

	assert(ps->size > 0); //为真继续，为假即<=0就会终止程序，并说明断言失败
	ps->size--;
}

void SeqlistPushFront(SL* ps, SLDataType x)
{
	/*    SeqlistCheckCapacity(ps);
	    // 挪动数据
	     int end = ps->size - 1;
	     while (end >= 0)
	     {
		    ps->a[end + 1] = ps->a[end];
		    --end; // 前置后置都可以
	     }
	     ps->a[0] = x;
	     ps->size++;
	*/
	SeqlistInsert(ps, 0, x);
}

void SeqlistPopFront(SL* ps)
{
	assert(ps->size > 0);
	int begin = 1;               //begin = 0
	while (begin < ps->size)     // size - 1
	{
		ps->a[begin - 1] = ps->a[begin];  // begin begin + 1
		++begin;
	}
	ps->size--;
	// 挪动数据
}

int SeqlistFind(SL* ps, SLDataType x)
{
	for (int i = 0; i < ps->size; i++)
	{
		if (ps->a[i] == x)
		{
			return i;
		}
	}
	return -1;
}

void SeqlistInsert(SL* ps, int pos, SLDataType x)
{
	/*   if (pos > ps->size || pos < 0)  越界
	    {
		   printf("pos invalid\n");
		   return;
	    }
	*/      //两种方法
	assert(pos >= 0 && pos <= ps -> size);

	SeqlistCheckCapacity(ps); // 插入考虑扩容
	int end = ps->size - 1;
	// 挪动数据
	while (end >= pos)
	{
		ps->a[end + 1] = ps->a[end];
		--end;
	}
	ps->a[pos] = x;
	ps->size++;
}

void SeqlistErase(SL* ps, int pos)
{
	assert(pos >= 0 && pos < ps->size);

	int begin = pos + 1;
	while (begin < ps->size)
	{
		ps->a[begin - 1] = ps->a[begin];
		++begin;    
	}
	ps->size--;
}