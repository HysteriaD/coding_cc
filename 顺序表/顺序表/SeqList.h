#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
//  #define N 1000
typedef int SLDataType; 

// 动态顺序表
typedef struct Seqlist
{
	SLDataType* a;
	int size;// 表示数组中存储了多少”个“数据
	int capacity; // 数组实际能存储数据的空间容量是多大
}SL;

// 静态特点：如果满了就不让插入 缺点：无法确定多少合适
// N给小了不够用， N给打了浪费
// 
//接口函数 -- 命名风格是跟着STL走的

void SeqlistPrint(SL* ps);// 打印顺序表
void SeqlistInit(SL* ps);// 初始化顺序表
void SeqlistDestory(SL* ps);// 将顺序表销毁
void SeqlistCheckCapacity(SL* ps);// 检查数据增容
void SeqlistPushBack(SL* ps, SLDataType x); //尾插
void SeqlistPopBack(SL* ps); //尾删
void SeqlistPushFront(SL* ps, SLDataType x); // 头插
void SeqlistPopFront(SL* ps); // 头删 
int SeqlistFind(SL* ps, SLDataType x);// 查找这个值的位置，找到了返回X下标位置，没有返回-1
void SeqlistInsert(SL* ps, int pos, SLDataType x);//在指定pos下标位置插入
void SeqlistErase(SL* ps, int pos);//删除pos位置的数据