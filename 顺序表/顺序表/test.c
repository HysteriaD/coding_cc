#include "Seqlist.h"

void TestSeqlist1()
{
	SL s1;
	SeqlistInit(&s1);
    SeqlistPushBack(&s1, 1);
	SeqlistPushBack(&s1, 2);
	SeqlistPushBack(&s1, 3);
	SeqlistPushBack(&s1, 4);
	SeqlistPushBack(&s1, 5);
	SeqlistPrint(&s1);

	SeqlistPopBack(&s1);
	SeqlistPopBack(&s1);
	SeqlistPrint(&s1);

	SeqlistDestory(&s1);
}

void TestSeqlist2()
{
	SL s1;
	SeqlistInit(&s1);
	SeqlistPushBack(&s1, 1);
	SeqlistPushBack(&s1, 2);
	SeqlistPushBack(&s1, 3);
	SeqlistPushBack(&s1, 4);
	SeqlistPushBack(&s1, 5);
	SeqlistPrint(&s1);

	SeqlistPushFront(&s1, 10);
	SeqlistPushFront(&s1, 20);
	SeqlistPushFront(&s1, 30);
	SeqlistPrint(&s1);

	SeqlistPopFront(&s1);
	SeqlistPopFront(&s1);
	SeqlistPrint(&s1);

	SeqlistDestory(&s1);
}

void TestSeqlist3()
{
	SL s1;
	SeqlistInit(&s1);
	SeqlistPushBack(&s1, 1);
	SeqlistPushBack(&s1, 2);
	SeqlistPushBack(&s1, 3);
	SeqlistPushBack(&s1, 4);
	SeqlistPushBack(&s1, 5);
	SeqlistPrint(&s1);

	SeqlistInsert(&s1, 2, 30);
	SeqlistPrint(&s1);

	int pos = SeqlistFind(&s1, 4);
	if (pos != -1)
	{
		SeqlistInsert(&s1, pos, 40);
	}
	SeqlistPrint(&s1);

	SeqlistDestory(&s1);
}

void TestSeqlist4()
{
	SL s1;
	SeqlistInit(&s1);
	SeqlistPushBack(&s1, 1);
	SeqlistPushBack(&s1, 2);
	SeqlistPushBack(&s1, 3);
	SeqlistPushBack(&s1, 4);
	SeqlistPushBack(&s1, 5);
	SeqlistPrint(&s1);

	int pos = SeqlistFind(&s1, 3);
	if (pos != -1)
	{
		SeqlistErase(&s1, pos);
	}
	SeqlistPrint(&s1);
	SeqlistDestory(&s1);
}

enum
{
	PushFront = 1,
	PopFront,
	PushBack,
	PopBack,
	Insert,
	Erase,
	Find,
	Print
};

// 菜单
void Menu()
{
	printf("*********************************\n");
	printf("*****    请选择你的操作:>   *****\n");
	printf("*****  1、头插    2、头删   *****\n");
	printf("*****  3、尾插    4、尾删   *****\n");
	printf("*****  5、插入    6、删除   *****\n");
	printf("*****  7、查找    8、打印   *****\n");
	printf("*****      -1、退出         *****\n");
	printf("*********************************\n");
}

int main()
{
	//TestSeqlist1();
	//TestSeqlist2();
	//TestSeqlist3();
	SL s1;
	SeqlistInit(&s1); // 初始化
	int input = 0;
	int x;
	int pos = 0;
	while (input != -1)
	{
		Menu();
		scanf("%d", &input);
		switch (input)
		{
		case PushFront:
			printf("请选择你要插入的数据，-1结束\n");
			do
			{
				scanf("%d", &x);
				if (x != -1)
				{
					SeqlistPushFront(&s1, x);
				}
			} while (x != -1);
			break;

		case PopFront:
			SeqlistPopFront(&s1);
			break;

		case PushBack:
			printf("请选择你要插入的数据，-1结束\n");
			do
			{
				scanf("%d", &x);
				if (x != -1)
				{
					SeqlistPushBack(&s1, x);
				}
			} while (x != -1);
			break;

		case PopBack:
			SeqlistPopBack(&s1);
			break;

		case Insert:
			printf("请选择你要插入的数据以及插入的位置，-1结束\n");
			do
			{
				scanf("%d %d", &pos, &x);
				if (x != -1)
				{
					SeqlistInsert(&s1, pos, x);
				}
			} while (x != -1);
			break;

		case Erase:
			printf("请选择你要删除的数据，-1结束\n");
			do
			{
				scanf("%d", &x);
				if (x != -1)
				{
					SeqlistErase(&s1, x);
				}
			} while (x != -1);
			break;

		case Find:
			printf("请选择你要查找的数据，-1结束\n");
			do
			{
				scanf("%d", &x);
				if (x != -1)
				{
					printf("找到了，下标为%d\n", SeqlistFind(&s1, x));
				}
				else
				{
					printf("没找到\n");
				}
			} while (x != -1);
			break;

		case Print:
			SeqlistPrint(&s1);
			break;
		default:
			printf("无此选项，请重新选择\n");
			break;
		}
	}
	SeqlistDestory(&s1);
	TestSeqlist4();
	return 0;
}