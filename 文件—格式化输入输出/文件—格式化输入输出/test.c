#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//struct S
//{
//	char name[20];
//	int age;
//	float score;
//};
//
//int main()
//{
//	struct S s = { "zhangsan", 20, 95.5f };
//	// 把s中的数据写到文件中
//	FILE* pf = fopen("test.txt", "w");
//	if (NULL == pf)
//	{
//		perror("fopen");
//		return 1;
//	}
//	// 写文件
//	fprintf(pf, "%s %d %.1f", s.name, s.age, s.score);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


struct S
	{
		char name[20];
		int age;
		float score;
	};

int main()
{
	struct S s = { 0 };
	FILE* pf = fopen("test.txt", "r");
	if (NULL == pf)
	{
		perror("fopen");
		return 1;
	}
	// 读文件
	fscanf(pf,"%s %d %f", s.name, &(s.age), &(s.score));
	printf("%s %d %f\n", s.name, s.age, s.score);
	fclose(pf);
	pf = NULL;
	return 0;
}