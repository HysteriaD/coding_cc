#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <assert.h>//memcpy返回类型是void*
void* my_memcpy(void* dest, void* src, size_t num)//不确定类型用void
{
	void* ret = dest;
	assert(dest);
	assert(src);
	while (num--)
	{
		*(char*)dest = *(char*)src;//void*指针不能直接解引用，因为要拷贝一个字节
		dest = (char*)dest + 1;    // 因此需要强制类型转换成char*
		src = (char*)src + 1;
	}
	return ret;
}

int main()
{
	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
	int arr2[10] = { 0 };
	my_memcpy(arr2, arr1, 20);//拷贝12345，一个整型4个字节5个20个字节
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		printf("%d ", arr2[i]);
	}

}