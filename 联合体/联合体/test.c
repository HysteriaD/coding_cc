#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
union Un
{
	char c;
	int i;
	double d;
};

int main()
{
	union Un un;
	printf("%p\n", &un);
	printf("%p\n", &(un.c));
	printf("%p\n", &(un.i));
	printf("%p\n", &(un.d));
}