#pragma once
#include <stdio.h>
#include <stdlib.h> 
#include <assert.h>

typedef int SLTDateType;

typedef struct SListNode
{
	SLTDateType data;
	struct SListNode* next;
}SLTNode;

void SListPrint(SLTNode* phead); // phead是一个指针，指向第一个节点
void SListPushBack(SLTNode** pphead, SLTDateType x);// 尾插
void SListPushFront(SLTNode** pphead, SLTDateType x);// 头插
void SListPopBack(SLTNode** pphead);//尾删
void SListPopFront(SLTNode** pphead);//头删

SLTNode* SListFind(SLTNode* phead, SLTDateType x);//查找也可实现修改
void SListInsert(SLTNode** pphead, SLTNode* pos, SLTDateType x);//插入(配合find一起用）
void SListErase(SLTNode** pphead, SLTNode* pos);//删除
void SListEraseAfter(SLTNode* pos);// 删除后一个
void SListDestroy(SLTNode** pphead);// 链表销毁
