//实现一个函数，可以左旋字符串中的k个字符
//abcdefghi->cdefghiab(旋转两个）
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
void left_move(char arr[], int k)
{
	int i = 0;
	int len = strlen(arr);
	k %= len;//防止重复
	for (i = 0; i < k; i++)
	{
		char tmp = arr[0];
		int j = 0;//把后面字符往前挪动一个位置
		for (j = 0; j < len - 1; j++)
		{
			arr[j] = arr[j + 1];
		}
		arr[len - 1] = tmp;
	}
}

int main()
{
	char arr[] = "abcdefghi";
	int k = 0;
	scanf("%d", &k);
	left_move(arr, k);
	printf("%s\n", arr);
	return 0;
}