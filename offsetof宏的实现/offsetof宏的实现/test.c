#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stddef.h>
struct S
{
	char a;
	int b;
	char c;
};

#define OFFSETOF(s_type, m_name)   (int)&(((s_type*)0)->m_name)//s_type*0指向了它的成员m_name再取地址强制类型 转换成int
                                                               //0是个地址，把它强制类型转换成结构体指针，然后访问它的成员
int main()
{
	printf("%d\n", OFFSETOF(struct S, a));
	printf("%d\n", OFFSETOF(struct S, b));
	printf("%d\n", OFFSETOF(struct S, c));
	//printf("%d\n", offsetof(struct S, a));
	//printf("%d\n", offsetof(struct S, b));
	//printf("%d\n", offsetof(struct S, c));
	return 0;
}