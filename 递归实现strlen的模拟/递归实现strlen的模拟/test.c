#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
int my_strlen(char* arr)
{
	if (*arr == '\0')
	{
		return 0;
	}
	else
	{
		return 1 + my_strlen(arr + 1);
	}
}
int main()
{
	char arr[] = "hellobit";
	printf("%d\n", my_strlen(arr));
	return 0;
}
