#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <assert.h>
void my_strncat(char* arr1, const char* arr2, int n)
{
	char* p = arr1;
	char* q = arr2;
	assert(arr1);
	assert(arr2);
	while (*p)
	{
		p++;
	}
	while (n--)
	{
		*p = *q;
		p++;
		q++;
	}
	*p = '\0';
	printf("%s", arr1);
	printf("\n");
}

int main()
{
	char arr1[20] = "hello \0xxxx";
	char arr2[] = "world";
	int num = 0;
	scanf("%d", &num);
	my_strncat(arr1, arr2, num);
	return 0;
}