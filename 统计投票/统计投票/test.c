#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
    char s = 0;
    int countA = 0;
    int countB = 0;
    while ((s = getchar()) != '0')
    {
        if (s == 'A')
            countA++;
        else
            countB++;
    }
    if (countA > countB)
        printf("A");
    else if (countA < countB)
        printf("B");
    else
        printf("E");
    return 0;
}