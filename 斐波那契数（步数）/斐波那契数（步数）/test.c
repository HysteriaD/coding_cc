#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
int main()
{
	int n = 0;
	scanf("%d", &n);
	int f1 = 0;
	int f2 = 1;
	int f3 = 0;
	while (1)
	{
		if (n == f2)
		{
			printf("0\n");
			break;
		}
		if (n < f2)
		{
			if (abs(f1 - n) < abs(f2 - n))
			{
				printf("%d\n", abs(f1 - n));
			}
			else
			{
				printf("%d\n", abs(f2 - n));
			}
			f1 = f2;
			f2 = f3;
			f3 = f1 + f2;
		}
	}
	return 0;
}