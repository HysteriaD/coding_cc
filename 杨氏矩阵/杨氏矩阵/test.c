//有一个数字矩阵，矩阵的每行从左到右时递增的
//从上到下时递增的
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
void find_yang(int arr[3][3], int k, int r, int c)
{
	int i = 0;
	int j = c - 1;
	int flag = 0;
	while (i <= r - 1 && j >= 0)
	{
		if (arr[i][j] < k)
		{
			i++;
		}
		else if (arr[i][j] > k)
		{
			j--;
		}
		else
		{
			flag = 1;
			printf("找到了，下标是 %d %d\n", i, j);
			break;
		}
	}
	if (flag == 0)
	{
		printf("找不到了\n");
	}
}

int main()
{
	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
	int k = 0;
	scanf("%d", &k);
	find_yang(arr, k, 3, 3);
	return 0;
}