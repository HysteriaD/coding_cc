//变种水仙花数例如 665=6*55+65*5 ，1461=1*461+14*61+146*1
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
int main()
{
	// 判断i是否为lily number
	// 12345
	// 12345%10=   5       12345/10= 1234
	// 12345%100=  45     12345/100= 123
	// 12345%1000= 345   12345/1000= 12
	// 12345%10000=2345 12345/10000= 1
	int i = 0;
	for (i = 10000; i <= 99999; i++)
	{
		int j = 0;
		int sum = 0;
		for (j = 1; j <= 4; j++)
		{
			int k = (int)pow(10, j);//j等于多少就是10的几次方，为后面除模10，100....做准备
			sum += (i % k) * (i / k);
		}
		if (sum == i)
		{
			printf("%d ", i);
		}
	}
	return 0;
}
