#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//#include <stdlib.h>
//int cmp(const void* a, const void* b)
//{
//	return *(int*)a - *(int*)b;
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int arr[100000];
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	qsort(arr, n, sizeof(int), cmp);
//	for (j = 0; j < n; j++)
//	{
//		if (arr[i] = arr[i + 1])
//		{
//			continue;
//		}
//		printf("%lld ", arr[i]);
//	}
//	return 0;
//}

#include <stdlib.h>

int cmp(const void* a, const void* b)
{
    return *(int*)a - *(int*)b;
}

int main()
{
    int n, count, i, j;
    scanf("%d", &n);
    int arr1[100000];
    count = 0;
    for (i = 0; i < n; i++)
    {
        scanf("%d", &arr1[i]);
    }
    qsort(arr1, n, sizeof(int), cmp);
    for (i = 0; i < n; i++)
    {
        if (arr1[i] == arr1[i + 1])              //相同则不输出
        {
            continue;
        }
        printf("%lld ", arr1[i]);
    }
    return 0;
}
