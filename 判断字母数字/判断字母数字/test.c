#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
    char m = 0;
    while (scanf("%c", &m) != EOF)
    {
        if (m == 10)continue;
        if ((m >= 65 && m <= 90) || (m >= 97 && m <= 122))
        {
            printf("%c is an alphabet.\n", m);
        }
        else
            printf("%c is not an alphabet.\n", m);
    }

}