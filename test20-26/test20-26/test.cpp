#define _CRT_SECURE_NO_WARNINGS
#include <iostream>

using namespace std;

#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int Status;
typedef char QElemType;
typedef struct QNode {
	QElemType data;
	struct QNode* next;
}QNode, * QueuePtr;
typedef struct {
	QueuePtr front;
	QueuePtr rear;
}LinkQueue;

Status InitQueue(LinkQueue& Q)
{
	Q.front = Q.rear = new QNode;
	Q.front->next = NULL;
	return OK;
}
Status EnQueue(LinkQueue& Q, QElemType e)
{
	QNode* p;
	p = new QNode;
	p->data = e;
	p->next = NULL;
	Q.rear->next = p;
	Q.rear = p;
	return OK;
}

Status DeQueue(LinkQueue& Q, QElemType& e)
{
	QNode* p;
	if (Q.front == Q.rear)
		return ERROR;
	p = Q.front->next;
	e = p->data;
	Q.front->next = p->next;
	if (Q.rear == p)
		Q.rear = Q.front;
	delete p;
	return OK;
}

Status QueueEmpty(LinkQueue Q)
{
	if (Q.front = Q.rear)
		return true;
	else
		return false;
}

Status QueueLength(LinkQueue Q)
{
	int i;
	if (Q.front != Q.rear)
	*(Q.front)++;
	i++;
	return i;
}

QElemType Gethead(LinkQueue Q)
{
	if (Q.front != Q.rear)
		return Q.front->next->data;
}

Status QueueTraverse(LinkQueue Q)
{
	if (Q.front != Q.rear)
	*(Q.front)++;
}

int main()
{
	LinkQueue q;
	char ch;
	InitQueue(q);
	EnQueue(q, 'a');
	EnQueue(q, 'b');
	EnQueue(q, 'c');
	DeQueue(q, ch);
	cout << ch << endl;
	DeQueue(q, ch);
	cout << ch << endl;
	DeQueue(q, ch);
	cout << ch << endl;
	return 0;
}