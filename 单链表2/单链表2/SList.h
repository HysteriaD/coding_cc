#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLTDateType;

typedef struct SListNode
{
	SLTDateType data;
	struct SListNode* next;
}SLTNode;

void SListPrint(SLTNode* phead);
void SListPushBack(SLTNode** pphead, SLTDateType x);
void SListPushFront(SLTNode* pphead, SLTDateType x);
void SListPopFront(SLTNode** pphead);
void SListPopBack(SLTNode** pphead);
void SListFind(SLTNode* phead);
void SListInset(SLTNode** pphead, SLTNode* pos, SLTDateType x);