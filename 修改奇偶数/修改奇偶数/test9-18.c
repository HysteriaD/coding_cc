#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int main(void)
{
    int n = 0;
    int a[9];//定义一个数还是偶数
    int count = 0;
    int result = 0;
    scanf("%d", &n);
    while (n != 0)
    {
        int m = n % 10;
        if (m % 2 == 0)
            a[count++] = 0;
        else
            a[count++] = 1;
        n = n / 10;
    }
    for (int i = count - 1; i >= 0; i--)
        result = result * 10 + a[i];
    printf("%d", result);
    return 0;
}
