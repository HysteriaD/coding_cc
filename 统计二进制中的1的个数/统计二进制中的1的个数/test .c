//按位&：两个二进制位都为1才是1，否则为0，所以两个数按位与的话，会直接求出输入的数的二进制位的最后一位是0还是1，为1结果就是1，为0结果就是0
//用这个原理我们可以计算出其最后1位是否为1，所以我们只要将该数循环右移，按位与1，就可以统计一共有多少个1了
//假设n = 15
//转换为二进制：00000000000000000000000000001010
//1的二进制   ：00000000000000000000000000000001
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int number(int n)
{
	int count = 0;
	for (int i = 0; i < 32; i++)
	{
		if (((n >> i) & 1) == 1)
		{
			count++;
		}
	}
	return count;
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int ret = number(n);
	printf("二进制中的1的个数为 %d\n", ret);
	return 0;
}
