#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
void change(char* a, int n)
{
	char* start = a;
	char* end = a+strlen(a)-1;//这里不能用num
	while (start < end)
	{
		char temp = *start;
		*start = *end;
		*end = temp;
		start++;
		end--;
	}
}
int main()
{
	char arr[] = "hello bit";
	char* p = arr;
	int num = sizeof(arr) / sizeof(arr[0]);
	change(arr, num);
	printf("%s\n", arr);
	return 0;
}