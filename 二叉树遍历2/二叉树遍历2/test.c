#include <stdio.h>
#include <stdlib.h>

typedef char BTDataType;
typedef struct BinaryTreeNode
{
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
	BTDataType data;
}BTNode;

BTNode* BuyNode(BTDataType x)
{
	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	if (node == NULL)
	{
		printf("malloc fail\n");
		exit(-1);
	}
	node->data = x;
	node->left = node->right = NULL;
	return node;
}

BTNode* CreatBinaryTree()
{
	BTNode* nodeA = BuyNode('A');
	BTNode* nodeB = BuyNode('B');
	BTNode* nodeC = BuyNode('C');
	BTNode* nodeD = BuyNode('D');
	BTNode* nodeE = BuyNode('E');
	BTNode* nodeF = BuyNode('F');

	nodeA->left = nodeB;
	nodeA->right = nodeC;
	nodeB->left = nodeD;
	nodeC->left = nodeE;
	nodeC->right = nodeF;

	return nodeA;
}

//二叉树前序遍历
void PreOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return; //return作用是回到调用的地方
	}
	printf("%c ", root->data); //递归
	PreOrder(root->left);
	PreOrder(root->right);
}

//二叉树中序遍历
void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	InOrder(root->left);
	printf("%c ", root->data);
	InOrder(root->right);
}

//二叉树后序遍历
void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%c ", root->data);
}



//二叉树节点个数
//遍历计数思想：多次调用存在问题
//int BinaryTreeSize(BTNode* root)
//{
//	if (root == NULL)
//	{
//		return;
//	}
//	static int count = 0;//将它放在静态区，不在内存中，但也不合适这个只能算一棵树，多棵树不行
//	++count; // ++count放在前面就是前序，放在中间就是中序，最后解释后序
//	BinaryTreeSize(root->left);
//	BinaryTreeSize(root->right);
//	return count;
//}

void BinaryTreeSize(BTNode* root, int* pn)
{
	if (root == NULL)
	{
		return;
	}
	++(*pn);
	BinaryTreeSize(root->left, pn);
	BinaryTreeSize(root->right, pn);
}//（节点计算）方法一

int BinaryTreeSize2(BTNode* root)
{
	return root == NULL ? 0 : BinaryTreeSize2(root->left) + BinaryTreeSize2(root->right) + 1;
}//（节点计算）方法二


//二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}

	if (root->left == NULL && root->right == NULL)
	{
		return 1;
	}

	return BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
}




int main()
{
	BTNode* root = CreatBinaryTree();
	PreOrder(root);
	printf("\n");
	InOrder(root);
	printf("\n");
	PostOrder(root);
	printf("\n");

	int n1 = 0;
	BinaryTreeSize(root, &n1);
	printf("TreeSize: %d\n", n1);
	printf("TreeSize: %d\n", BinaryTreeSize2(root));
	printf("TreeLeafSize: %d\n", BinaryTreeLeafSize(root));
	return 0;
}