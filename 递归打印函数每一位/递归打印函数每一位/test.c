#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
void print(unsigned int n)
{
	if (n < 10)
		printf("%d ", n);
	else
	{
		print(n / 10);
		printf("%d ", n % 10);
	}
}
int main()
{
	unsigned int num = 0;//定义一个无符号的整数
	scanf("%u", &num);
	print(num);//按照顺序打印num的每一位
	return 0;
}
//递归的两个必要条件：
//存在限制条件，当满足这个限制条件的时候，递归便不再继续
//每次递归调用之后越来越接近这个限制条件。