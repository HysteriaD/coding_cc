#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
enum Color
{
	//枚举的可能性
	//每一个可能的取值是常量
	RED = 5,
	GREEN = 7,
	BLUE = 10
};

int main()
{
	printf("%d\n", sizeof(enum Color));
	return 0;
}