#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int n = 0;
	scanf("%d", &n);//输入n
	int i = 0;
	int arr[n];
	for (i = 0; i < n; i++)//输入第一行
	{
		scanf("%d", &arr[i]);
	}
	//输入删除的数字
	int del = 0;
	scanf("%d", &del);
	//方法二
	i = 0;
	int j = 0;
	for (i = 0; i < n; i++)
	{
		if (arr[i] != del)
		{
			arr[j] = arr[i];
			j++;
		}
	}
	for (i = 0; i < j; i++)
	{
		printf("%d ", arr[i]);
	}

// 方法一：for (i = 0; i < n; i++)
//    {
//    if (arr[i] != del)        
//	  printf("%d ", arr[i]);
//    }
	return 0;
}