#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int my_atoi(const char* str)
{
	int n = 0;
	while (*str != '\0')
	{
		n = n * 10 + (*str - '0');
		str++;
	}
	return n;
}

int main()
{
	char arr[100] = "12345";
	int ret = my_atoi(arr);
	printf("%d\n", ret);
	return 0;
}