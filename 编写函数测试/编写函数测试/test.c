#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
    char* solve(char* s, char* t)
    {
        int slen = strlen(s);
        int tlen = strlen(t);
        int rlen = slen + tlen;
        char* r = (char*)malloc(rlen + 1);
        int i, j;
        char sbit, tbit, carry, rbit;
        memset(r, '0', rlen);//将前rlen个单位定义为0
        r[rlen] = '\0';//结尾加结束符号
        /* 执行乘法运算
         *  123   (t)
         *x 456   (s)
         *-------
         *  518   (r)
         */
        for (i = 0; i < slen; i++) {
            sbit = s[slen - i - 1];
            carry = 0;

            for (j = 0; j < tlen; j++) {
                tbit = t[tlen - j - 1];
                rbit = r[rlen - j - i - 1];

                carry = (rbit - '0') + carry
                    + (sbit - '0') * (tbit - '0');
                r[rlen - j - i - 1] = (carry % 10) + '0';
                carry /= 10;
            }

            r[rlen - tlen - i - 1] += carry;
        }

        if (carry == 0) {
            return &r[1];
        }
        return r;
    }
