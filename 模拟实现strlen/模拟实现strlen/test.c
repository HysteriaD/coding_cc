#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <assert.h>
size_t my_strlen(const char* str)
{
	assert(str);
	char* start = str;
	char* end = str;
	while (*end != '\0')
	{
		end++;
	}
	return end - start;
}

int main()
{
	char arr[] = "abcdef";
	int len = my_strlen(arr);
	printf("%d\n", len);
}