#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <assert.h>
int my_strcmp(const char* s1, const char* s2)
{
    assert(s1 && s2);
    while (*s1 == *s2)
    {
        s1++;
        s2++;
        if (*s1 == *s2)
        {
            return 0;
        }
    }
    if (*s1 > *s2)
        return 1;
    else
        return -1;//如果s1和s2相等就进入上一个循环
}                 //因而不用考虑相等的情况

int main()
{
    /*char arr1[] = "abcdef";
    char arr2[] = "abq";
    int ret = strcmp(arr1, arr2);
    printf("%d\n", ret);
    return 0;*/
    char arr1[] = "abc";
    char arr2[] = "abc";
    int ret = my_strcmp(arr1, arr2);
    if (ret > 0)
        printf("arr1 > arr2\n");
    else if (ret < 0)
        printf("arr1 < arr2\n");
    else
        printf("arr1 = arr2\n");
}