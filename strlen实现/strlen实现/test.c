#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<assert.h>
int my_strlen(const char* str)
{
    assert(str != NULL);
    int count = 0;
    while (*str)
    {
        count++;
        str++;
    }
    return count;
}
int main()
{
    char str[] = "abcdef";
    int len = my_strlen(str);
    printf("%d", len);
    return 0;
}