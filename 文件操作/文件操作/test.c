#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	//打开文件
	FILE* pf = fopen("test.txt", "r");// fopen打开文件，从内存中分配一块
	if (NULL == pf)
	{
		perror("fopen");
		return 1;
	}

	//写文件
	/*fputc('a', pf);
	fputc('b', pf);
	fputc('c', pf);*/
	//int i = 0;
	/*for (i = 0; i < 26; i++)
	{
		fputc('a' + i, pf);
	}*/
	//读文件
	/*int ch = fgetc(pf);
	printf("%c\n", ch);

	ch = fgetc(pf);
	printf("%c\n", ch);*/
	int ch = 0;
	while ((ch = fgetc(pf)) != EOF)
	{
		printf("%c ", ch);
	}

	//关闭文件
	fclose(pf);
	pf = NULL;
	return 0;
}