//  比如说We are happy替换后变为We%20are%20happy
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

void replaceSpace(char* str, int length) {
    //统计空格的个数
    int space_count = 0;
    char* cur = str;
    while (*cur)
    {
        if (*cur == ' ')
        space_count++;
        cur++;
    }
    //计算end1,end2
    char* end1 = str + length;
    char* end2 = str + length + 2 * space_count;
    while (end1 != end2)
    {
        if (*end1 != ' ')
        {
            *end2-- = *end1--;
        }
        else
        {
            *end2-- = '0';
            *end2-- = '2';
            *end2-- = '%';
            end1--;
        }
    }
}


int main()
{
    char arr[40] = "we are happy";
    int len = strlen(arr);
    replaceSpace(arr, len);
    printf("%s\n", arr);
    return 0;
}