#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int arr[] = { 1,2,2,3,3,1,4 };
	int dog = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	for (int i = 0; i < sz; i++)
	{
		dog ^= arr[i];
	}
	printf("%d\n", dog);
	return 0;
}