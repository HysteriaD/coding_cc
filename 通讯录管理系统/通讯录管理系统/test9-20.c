#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "contact.h"

void menu()
{
	printf("************************************\n");
	printf("******* 1.add     2.del     ********\n");
	printf("******* 3.search  4.modify  ********\n");
	printf("******* 5.show    6.sort    ********\n");
	printf("*******        0.exit       ********\n");
	printf("************************************\n");
}

int main()
{
	int input = 0;
	struct Contact con;
	InitContact(&con);// 初始化通讯录
	do 
	{
		// 第一步，创建通讯录系统
		//struct PeoInfo data[100];//data用来存放通讯录里面的数据
		//int sz = 0;//表示通讯录中还没有信息，sz记录通讯录有几个信息
		//假设有n个信息再放一个应该放在下标为n的位置上
		menu();
		printf("请选择>：");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			AddContact(&con);
			break;
		case 2:
			DelContact(&con);
			break;
		case 3:
			SearchContact(&con);
			break;
		case 4:
			ModifyContact(&con);
			break;
		case 5:
			ShowContact(&con);
			break;
		case 6:
			break;
		case 0:
			SortContact(&con);
			break;
		default:
			break;
		}
	} while (input);
	return 0; 
}