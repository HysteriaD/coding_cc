#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	FILE* pf = fopen("test.txt", "r");
	if (pf == NULL)
	{
		perror("fopen");
		return 1;
	}

	// 写文件一行一行写
	//fputs("hello\n", pf);
	//fputs("woshihulianwangzhishen", pf);

	// 一行一行读文件
	char arr[] = "123456789";
	fgets(arr, 20, pf);
	printf("%s", arr);

	fgets(arr, 20, pf);
	printf("%s", arr);

	fclose(pf);
	pf = NULL;
}