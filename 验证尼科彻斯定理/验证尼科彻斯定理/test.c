// 任何一个整数m的立方都可以写成m个连续奇数之和
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
// m的起始奇数值等于m*（m-1）+1
int main()
{
    int m = 0;
    while (scanf("%d", &m) != EOF)
    {
        int start = m * (m - 1) + 1;
        int i = 0;
        int string = 0;
        printf("%d", start);
        for (i = 1; i < m; i++)
        {
            string = start + 2;
            start += 2;
            printf("+%d", string);
        }
    }
}