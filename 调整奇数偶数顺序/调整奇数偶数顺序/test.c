#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
void change(int arr[], int len) 
{
	int left = 0;
	int right = len - 1;
	int temp = 0;
	while (left < right) 
	{
		//向右寻找，直到找到偶数跳出循环
		while (arr[left] % 2 == 1) 
		{
			left++;
		}
		//向左寻找，直到找到奇数跳出循环
		while (arr[right] % 2 == 0) 
		{
			right--;
		}
		//当left < right时，将左边的偶数和右边的奇数交换
		if (left < right) 
		{
			temp = arr[left];
			arr[left] = arr[right];
			arr[right] = temp;
			left++;
			right--;
		}
	}
}
int main() {
	int arr[] = { 1,2,3,4,5};
	int len = sizeof(arr) / sizeof(arr[0]);
	change(arr, len);
	for (int i = 0; i < len; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
	return 0;
}