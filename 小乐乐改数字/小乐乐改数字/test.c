#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
int main()
{
	int n = 0;
	scanf("%d", &n);
	int sum = 0;
	int i = 0;
	while (n)
	{
		int bit = n % 10;
		if (bit % 2 == 1)
		{
			sum += 1 * pow(10, i);
		}
		n /= 10;
		i++;
	}
	printf("%d", sum);
}