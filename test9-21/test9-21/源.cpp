//程序名称；线性表的链式存储与实现 
//编译环境；Dev-c++ 5.5.3 
//作者；徐骏羽
//最后修改； 

#include <iostream>

using namespace std;

#define ERROR 0
#define OK 1

typedef int ElemType;
typedef struct LNode {
	ElemType data;
	struct LNode* next;
} LNode, * LinkList;
typedef int Status;


void CreateList_H(LinkList& L, int n)
/*逆位序输入n个元素的值，建立带表头节点
的单链表L*/
{
	int i;
	LNode* p;
	L = new LNode;
	L->next = NULL;
	for (i = 0; i < n; ++i)
	{
		p = new LNode;
		p->next = L->next;
		L->next = p;
	}
}

void Print(LinkList L)
/*依次输出单链表L的所有数据元素*/
{
	LNode* p;
	p = L->next;
	while (p != NULL) {
		cout << p->data << " ";
		p = p->next;
	}
}

Status ListInsert(LinkList& L, int i, ElemType e)
{
	LNode* p = L, * s;
	int j = 0;
	while (p && j < i - 1)
	{
		p = p->next;
		++j;
	}
	if (!p || j > i - 1)
		return ERROR;
	s = new LNode;
	s->data = e;
	s->next = p->next;
	p->next = s;
	return OK;
}

int main()
{
	int n, len;
	LinkList L, p;
	cout << "Input n:";
	cin >> n;
	CreateList_H(L, n);
	ListInsert(L, 1, 10);
	Print(L);
	return 0;
}