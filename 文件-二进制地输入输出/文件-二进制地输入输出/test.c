#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//struct S
//{
//	char name[20];
//	int age;
//	float score;
//};
//
//int main()
//{
//	struct S s = { "zhangsan", 20, 99.5f };
//	FILE* pf = fopen("test.txt", "wb");// wb二进制的写
//	if (NULL == pf)
//	{
//		perror("fopen");
//		return 1;
//	}
//	// 写文件
//	fwrite(&s, sizeof(s), 1, pf);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

struct S
{
	char name[20];
	int age;
	float score;
};

int main()
{
	struct S s = { 0 };
	FILE* pf = fopen("test.txt", "rb");// rb二进制的读
	if (NULL == pf)
	{
		perror("fopen");
		return 1;
	}
	// 写文件
	fread(&s, sizeof(s), 1, pf);
	printf("%s %d %f\n", s.name, s.age, s.score);
	fclose(pf);
	pf = NULL;
	return 0;
}