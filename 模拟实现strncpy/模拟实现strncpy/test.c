#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
void my_strncpy(char* dest, const char* src, int n)
{
	while (n--)
	{
		*dest++ = *src++;
	}
}

int main()
{
	char arr1[20] = { 0 };
	char arr2[] = "abcdef";
	int n = 0;
	scanf("%d", &n);
	my_strncpy(arr1, arr2, n);
	printf("%s\n", arr1);
	return 0;
}