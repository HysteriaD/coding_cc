/* 有一个长度为 n 的非降序数组，比如[1,2,3,4,5]，将它进行旋转，即把一个数组最开始的若干个元素搬到数组的末尾，
* 变成一个旋转数组,比如变成[3，4，5，1，2] ,或者{4，5，1，2，3]
* 这样的，给定这样一个旋转数组，求数组中的最小值。
* 
* 数据范围1<=n<=1000，数组中任意元素的值0<=val<=10000
* 要求空间复杂度：O(1),时间复杂度：O(logn)
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
int minNumberInRotateArray(int* rotateArray, int rotateArraylen)
{
	int left = 0;
	int right = rotateArraylen - 1;
	int mid = (left + right) / 2;
	while (left < right)
	{
		if (rotateArray[mid] > rotateArray[right])
		{
			left = mid + 1;
		}
		else if (rotateArray[mid] < rotateArray[right])
		{
			right = mid;
		}
		else
			right--;
	}
	return rotateArray[left];
}

int main()
{
	int n = 0;
	int rotateArray[5];
	int i = 0;
	int rotateArraylen = strlen(rotateArray);
	for (i = 0; i < 5; i++)
	{
		scanf("&d", rotateArray);
	}
	int ret = minNumberInRotateArray(rotateArray, rotateArraylen);
	printf("%d ", ret);
}
