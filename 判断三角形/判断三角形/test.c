#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
void tran(int max, int medium, int min)
{
    if ((min + medium) - max > 0)
    {
        if (max == medium && medium == min)
        {
            printf("Equilateral triangle!\n");
        }
        else if (max == medium || medium == min || max == min)
        {
            printf("Isosceles triangle!\n");
        }
        else
            printf("Ordinary triangle!\n");
    }
    else
        printf("Not a triangle!\n");
}
int main()
{
    int a, b, c;
    while (scanf("%d %d %d", &a, &b, &c) != EOF)
    {
        if (a > b && a > c )
        {
            tran(a, b, c);
        }
        else if (b > c && b > a)
        {
            tran(b, a, c);
        }
        else if (c>b && c > a)
        {
            tran(c, a, b);
        }
        else
            tran(a, b, c);
    }
    return 0;
}