#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
void print(int arr[], int sz)
{
	int i = 0;
	for(i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}
int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8 };
	print(arr, 10);
	return 0;//函数如果不写返回值，函数默认回返回一个值
}//有些编译器返回的是：最后一条指令的结果