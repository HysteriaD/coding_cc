#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int main() {
	int a;
	while (scanf("%d", &a) != EOF) 
	{
		int i, k;
		for (k = 0; k < a; k++) 
		{
			for (i = 0; i < (a - k - 1); i++)
			{
				printf("  ");
			}
			for (i = 0; i <= k; i++) 
			{
				printf("* ");
			}
			printf("\n");
		}
	}
	return 0;
}
