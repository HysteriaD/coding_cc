#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int n = 0;
	int i = 0;//行，有几行就有几项
	scanf("%d", &n);
	for (i = 1; i <=n; i++)//打印行
	{
		int j = 0;
		for (j = 1; j <= i; j++)//打印列，i=多少就有多少列
		{
			printf("%d*%d=%-2d ", i, j, i * j);//%-2d表示左对齐两格，%2d表示右对齐两格
		}
		printf("\n");
	}
}