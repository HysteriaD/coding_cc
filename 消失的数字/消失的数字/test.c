//从0到n找出缺失的那一个
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int missingNUmber(int* nums,int numsSize)
{
    int x = 0;
    int i = 0;
    for (i = 0; i <= numsSize; i++)
    {
        x ^= i;
    }
    for (i = 0; i < numsSize; i++)
    {
        x ^= nums[i];
    }
    return x;
}

int main()
{
    int nums[100];
    int numSize = sizeof(nums) / sizeof(nums[0]);
    int ret = missingNUmber(nums, numSize);
    printf("%d ", ret);
}