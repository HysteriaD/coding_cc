#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

// 假设实现大堆
typedef int HPDataType;
typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}HP;

void HeapInit(HP* hp);
void HeapDestroy(HP* hp);
void HeadPush(HP* hp, HPDataType x);
void HeapPop(HP* hp);// 删除数据是指删除堆顶的数据即删除这棵树的根
void HeapPrint(HP* hp);
bool HeapEmpty(HP* hp);
int HeapSize(HP* hp);
void Swap(HPDataType* px, HPDataType* py);