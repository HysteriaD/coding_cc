#include <stdio.h>
#include <stdbool.h>

struct TreeNode 
{
	int val;
	struct TreeNode* left;
	struct TreeNode* right;
};

bool _isSymmetricTree(struct TreeNode* root1, struct TreeNode* root2)
{
	if (root1 == NULL && root2 == NULL)
		return true;

	if (root1 == NULL || root2 == NULL)
		return false;

	if (root1->val != root2->val)
		return false;

	return _isSymmetricTree(root1->left, root2->right) &&
		_isSymmetricTree(root1->right, root2->left);
}

bool isSymmetric(struct TreeNode* root)
{
	if (root == NULL)
		return true;

	return _isSymmetricTree(root->left, root->right);
}