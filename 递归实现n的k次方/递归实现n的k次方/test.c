#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int power(int n, int j)
{
	if (j <= 0)
		return 1;
	else
		return n * power(n, j - 1);
}
int main()
{
	int n = 0;
	int j = 0;
	scanf("%d%d", &n, &j);
	int result = power(n, j);
	printf("%d\n", result);
	return 0;
}