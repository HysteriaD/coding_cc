#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//int check_sys()
//{
//	int num = 1;
//	char* p = (char*)&num;//强制类型转换，本来num是int*
//
//	if (*p == 1)
//		return 1;
//	else
//		return 0;
//}

//第二种方法
// if (*（char*)num == 1
// return *(char*)&num

int check_sys()
{
	union Un
	{
		char c;
		int i;
	}u;
	u.i = 1;
	return u.c;
}

int main()
{
	int ret = check_sys();
	if (ret == 1)
		printf("小端\n");
	else
		printf("大端\n");
	return 0;
}