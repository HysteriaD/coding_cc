#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int judge(int year)
{
	if (year % 4 == 0 && year % 100 != 0)
	{
		return 1;
	}
	else if (year % 400 == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
int main()
{
	int year = 0;
	scanf("%d", &year);
	int j = 0;
	j = judge(year);
	if (j == 1)
	{
		printf ("是润年\n");
	}
	else
	{
		printf("是平年\n");
	}
}