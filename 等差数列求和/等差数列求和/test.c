#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        int start = 2;
        int end = start + 3 * (n - 1);
        int sum = 0;
        sum = (start + end) * n / 2;
        printf("%d", sum);
    }
    return 0;
}