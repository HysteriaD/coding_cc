#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int DigitSum(n)
{
	if (n > 0) 
	{
		return n % 10 + DigitSum(n / 10);
	}
	return 0;
}
void main()
{
	int n = 0;
	scanf("%d", &n);
	int sum = DigitSum(n);
	printf("%d\n", sum);
}
