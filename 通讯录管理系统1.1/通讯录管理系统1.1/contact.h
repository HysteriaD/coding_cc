#pragma once

#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#define MAX 100
#define MAX_NAME 20
#define MAX_SEX 5
#define MAX_TELE 12
#define MAX_ADDR 30


// 表示一个人的信息
struct PeoInfo
{
	char name[MAX_NAME];
	char sex[MAX_SEX];
	char tele[MAX_TELE];
	int age;
	char addr[MAX_ADDR];
};

struct Contact
{
	struct PeoInfo data[MAX];
	int sz;
};

void InitContact(struct Contact* pc);// 初始化通讯录

void AddContact(struct Contact* pc);// 增加人的信息到通讯录

void ShowContact(const struct Contact* pc);// 显示通讯录中的信息

void DelContact(struct Contact* pc);// 删除指定联系人

void SearchContact(const struct Contact* pc);// 查找指定联系人

void ModifyContact(struct Contact* pc);// 修改联系人

void SortContact(struct Contact* pc);// 排序通讯录中的信息- 年龄



