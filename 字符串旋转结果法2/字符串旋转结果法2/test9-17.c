#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
int is_left_move(char arr1[], char arr2[])
{
	int len1 = strlen(arr1);
	int len2 = strlen(arr2);
	if (len1 != len2)
		return 0;
	strncat(arr1, arr1, len1);
	char* m = strstr(arr1, arr2);
	if (m == NULL)
		return 0;
	else
		return 1;
}

int main()
{
	char arr1[20] = "ABCDEF";
	char arr2[] = "BCDEFA";
	int ret = is_left_move(arr1, arr2);
	if (1 == ret)
		printf("YES\n");
	else
		printf("NO\n");
	return 0;
}