#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
void move_even_odd(int arr[], int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left < right)
	{
		//从前往后找一个偶数就停下来
		while ((arr[left] % 2 == 1) && (arr[left]%2==1))
		{
			left++;
		}
		//从后往前再找一个奇数停下来
		while ((arr[right] % 2 == 0) && (arr[right]%2==0))
		{
			right--;
		}
		if (left < right)
		{
			int tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;
			left++;
			right--;
		}
	}
}
void  print(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}
int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	print(arr, sz);
	move_even_odd(arr, sz);
	print(arr, sz);
	return 0;
}