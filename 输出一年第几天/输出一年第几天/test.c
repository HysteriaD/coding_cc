#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
    int a[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    int year = 0;
    int month = 0;
    int day = 0;
    scanf("%d %d %d", &year, &month, &day);
    if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
    {
        a[1]++;
    }
    int count = 0;
    for (int i = 0; i < month - 1; i++)
    {
        count += a[i];
    }
    count += day;
    printf("%d", count);
    return 0;
}