#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//     0 1 2 3
//   0 - - - *     
//   1 - - * -      我们可以看出"行+列"的和为3时才打印*
//   2 - * - -
//   3 * - - -
int main()
{
	int n = 0;
	scanf("%d", &n);
	int i = 0;
	int j = 0;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			if (i + j == n - 1)
			{
				printf("* ");
			}
			else
				printf(" ");
		}
		printf("\n");
	}
	return 0;
}