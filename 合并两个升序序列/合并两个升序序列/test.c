#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
	int main()
	{
		int n = 0;
		int m = 0;
		int arr1[100] = { 0 };
		int arr2[100] = { 0 };
		scanf("%d %d", &n, &m);
		int i = 0;
		for (i = 0; i < n; i++)
		{
			scanf("%d", &arr1[i]);
		}
		printf("请输入第二个数组元素：\n");
		for (i = 0; i < m; i++)
		{
			scanf("%d", &arr2[i]);
		}
		i = 0;    //用来遍历arr1数组
		int j = 0;//用来遍历arr2数组
		while (i < n && j < m)//对相应序列的前 n 个或者前 m 个进行有序输出
		{
			if (arr1[i] < arr2[j])
			{
				printf("%d ", arr1[i]);
				i++;
			}
			else
			{
				printf("%d ", arr2[j]);
				j++;
			}
		}
		if (i == n)// n 的个数较大，由于开始就是有序序列，直接对之后的序列进行输出
		{
			for (; j < m; j++)
			{
				printf("%d ", arr2[j]);
			}
		}
		else
		{
			for (; i < n; i++)// m 的个数较大，由于开始就是有序序列，直接对之后的序列进行输出
			{
				printf("%d ", arr1[i]);
			}
		}
		return 0;
	}