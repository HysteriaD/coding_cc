#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

#define SQUARE(x) x*x

int main()
{
	printf("%d\n", SQUARE(5 + 1));
	return 0;
}