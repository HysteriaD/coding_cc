#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
int is_left_move(char arr1[], char arr2[])
{
	int i = 0;
	int len = strlen(arr1);
	for (i = 0; i < len; i++)
	{
		char tmp = arr1[0];
		int j = 0;
		for (j = 0; j < len - 1; j++)
		{
			arr1[j] = arr1[j + 1];
		}
		arr1[len - 1] = tmp;
		if (strcmp(arr1, arr2) == 0)
		{
			return 1;
		}
	}
	return 0;
}

int main()
{
	char arr1[] = "ABCDEF";
	char arr2[] = "CDEFAB";
	int ret = is_left_move(arr1, arr2);
	if (ret == 1)
		printf("YES\n");
	else
		printf("NO\n");
	return 0;
}